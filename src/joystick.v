module joystick (
  input clk,
  input rst,
  input up,
  input down,
  input left,
  input right,
  input fire1,
  input fire2,

  output reg [7:0] joy_data
);

always @(posedge clk )
begin
	joy_data[7] = ~up;
	joy_data[6] = ~down; 
	joy_data[5] = ~left;
	joy_data[4] = ~right;
	joy_data[3] = ~fire1;
	joy_data[2] = ~fire2;
	joy_data[1] = 1'b0;
	joy_data[0] = 1'b0;
end

endmodule
