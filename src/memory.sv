module videoBuffer(rclock, wclock, read, write,raddr, rdata, waddr, wdata);

input  rclock;
input  wclock;
input  read;
input  write;
input  [15:0] raddr;
output [15:0] rdata;
input  [15:0] waddr;
input  [15:0] wdata;

reg [15:0] buffer[0:39999]; // x, y and rgb buffer
reg [15:0] temp_data;


reg [8:0] x,y;

//This paints a pattern at the begining to check that everything is working
initial begin
  for (y=0;y<200;y=y+1)
	for (x=0;x<200;x=x+1)
		buffer[200*y+x] = 16'h0000D69A; //Light grey
end


always @(posedge rclock)
begin
	if (read == 1'b1)
		begin
			temp_data <= buffer[raddr];
		end	
end

always @(posedge wclock)
begin
	if (write == 1'b1)
		begin
			buffer[waddr] <= wdata;
		end
end

//assign rdata = read & !write ? temp_data: 'hz; 
assign rdata = temp_data; 

endmodule