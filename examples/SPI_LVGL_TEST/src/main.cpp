/**
BSD 3-Clause License

Copyright (c) 2021, Guillermo Amat
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/
#include <Arduino.h>
#include <SPI.h>
#include <lvgl.h>

#include "spidisplay.h"

// Define ALTERNATE_PINS to use non-standard GPIO pins for SPI bus

#ifndef RASPBERRY_PI_PICO
#ifdef ALTERNATE_PINS
  #define VSPI_MISO   2
  #define VSPI_MOSI   4
  #define VSPI_SCLK   0
  #define VSPI_SS     33

  #define HSPI_MISO   26
  #define HSPI_MOSI   27
  #define HSPI_SCLK   25
  #define HSPI_SS     32
#else
  #define VSPI_MISO   MISO
  #define VSPI_MOSI   MOSI
  #define VSPI_SCLK   SCK
  #define VSPI_SS     SS

  #define HSPI_MISO   12
  #define HSPI_MOSI   13
  #define HSPI_SCLK   14
  #define HSPI_SS     15
#endif
#else
  #define  MISO 12
  #define  MOSI 15
  #define  SCK  14
  #define  SS   13
#endif

#if CONFIG_IDF_TARGET_ESP32S2
#define VSPI FSPI
#endif

#ifdef RASPBERRY_PI_PICO
#define LED 25
#else
#define LED 2
#endif

bool anotherValue=true;

char strFPS[12];
unsigned long frameTime;
unsigned long frames;
SPIDisplay *disp;

//uninitalised pointers to SPI objects
SPIClass * vspi = NULL;
SPIClass * hspi = NULL;

typedef struct t_data{
  uint16_t x;
  uint16_t y;
  uint8_t color;
} ;


lv_obj_t * line1;
lv_obj_t * label;
lv_obj_t * labelFPS;

lv_color_t color_red; 
lv_color_t color_green; 

static lv_style_t style_line;
static lv_style_t style_line2;

void setup() {
  frames = 0;
  Serial.begin(9600);
  while (!Serial);
  delay(200);
  
  while(Serial.available() == 0) {
    }

  pinMode(LED, OUTPUT); //HSPI SS
  digitalWrite(LED, LOW);

  lv_init();

  disp = SPIDisplay::getDisplay();

   label = lv_label_create( lv_scr_act() );
   lv_label_set_text( label, "Hello Arduino! (V8.0.X)" );
   lv_obj_align( label, LV_ALIGN_CENTER, 0, 0 );


   labelFPS = lv_label_create( lv_scr_act() );
   lv_label_set_text( labelFPS, "0" );
   lv_obj_align( labelFPS, LV_ALIGN_TOP_LEFT, 0, 0 );

   /*Create an array for the points of the line*/
  static lv_point_t line_points[] = { {5,90}, {95,90} };

  color_red = lv_color_hex(0xff0000); 
  color_green = lv_color_hex(0x00ff00); 
  /*Create style*/
  lv_style_init(&style_line);
  lv_style_set_line_width(&style_line, 8);
  lv_style_set_line_color(&style_line, color_red);
  lv_style_set_line_rounded(&style_line, true);


  lv_style_init(&style_line2);
  lv_style_set_line_width(&style_line2, 8);
  lv_style_set_line_color(&style_line2, color_green);
  lv_style_set_line_rounded(&style_line2, true);

  /*Create a line and apply the new style*/
  line1 = lv_line_create(lv_scr_act());
  lv_line_set_points(line1, line_points, 2);     /*Set the points*/
  lv_obj_add_style(line1, &style_line, 0);
  lv_obj_center(line1);

}

// the loop function runs over and over again until power down or reset
void loop() {
  frames++;
  anotherValue=!anotherValue;

 
  if (anotherValue){
    lv_label_set_text(label, "Hello");
    //lv_obj_add_style(line1, &style_line, 0);
  }else{
    lv_label_set_text(label, "World");
    //lv_obj_add_style(line1, &style_line2, 0);
  }
  
  sprintf(strFPS, "%lu FPS", 1000*frames/millis());
  lv_label_set_text(labelFPS, strFPS);
  lv_timer_handler();
}