/**
BSD 3-Clause License

Copyright (c) 2021, Guillermo Amat
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#ifndef MAIX_SPI_DISPLAY_H
#define MAIX_SPI_DISPLAY_H

#ifdef USE_SPI_HDMI

#define LV_COLOR_DEPTH 8
#define LV_COLOR_16_SWAP 0

#include <Arduino.h>
#include <lvgl.h>
#include <Ticker.h>
#include "spisingleton.h"

#ifndef LV_HOR_RES_MAX
#define LV_HOR_RES_MAX 200
#endif

#define LVGL_TICK_PERIOD 40


class SPIDisplay{
private:
  //Ticker tick; // timer for interrupt handler 
  lv_disp_draw_buf_t disp_buf;
  lv_disp_drv_t disp_drv;
  lv_color_t buf[LV_HOR_RES_MAX * 10];

  bool locked; //Mutex access to SPI


  //! Singleton properties
  static SPIDisplay *spiDisplayInstance; //! The only instance
  SPIDisplay(); //! Private constructor to avoid more instances
  
public:
  static void my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p);
  static void lv_tick_handler(void);
  bool read_encoder(lv_indev_drv_t * indev, lv_indev_data_t * data);

  //! Singleton method to get the instance
  static SPIDisplay* getDisplay();
};

#endif //USE_SPI_HDMI
#endif //MAIX_SPI_DISPLAY_H
