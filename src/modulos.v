/*
 * This file is part of "Modulos de entrenamiento para FPGAs"
 * Copyright (c) 2018 Miguel Angel Rodriguez Jodar.
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

`timescale 1ns / 1ns
`default_nettype none

module display (
  input wire clk,
  // Acceso a la ultima tecla pulsada
  output wire evento_teclado,
  output wire [7:0] scancode,
  output wire soltada,
  output wire extendida,
  
  // Acceso a la pantalla
  input wire [5:0] ri,
  input wire [5:0] gi,
  input wire [5:0] bi,
  output wire [10:0] posx,
  output wire [10:0] posy,
  output wire display_activo,  
    
  // Interface teclado
  input wire clkps2,
  input wire dataps2,
  
  // Interface VGA
  output wire hblank,
  output wire [5:0] r,
  output wire [5:0] g,
  output wire [5:0] b,
  output wire hs,
  output wire vs
  );
  
  parameter YINIT = 392;
  parameter FONTFILE = "fuente_hexadecimal_ibm.hex";
  
  wire [10:0] hc, vc;
  wire den;
  
  wire [5:0] rdigits, gdigits, bdigits;
  wire [5:0] rleds, gleds, bleds;
  wire [5:0] rswitch, gswitch, bswitch;
  
  ps2_port el_teclado (
    .clk(clk),  // se recomienda 1 MHz <= clk <= 600 MHz
    .enable_rcv(1'b1),  // habilitar la maquina de estados de recepcion
    .kb_or_mouse(1'b0),
    .ps2clk_ext(clkps2),
    .ps2data_ext(dataps2),
    .kb_interrupt(evento_teclado),  // a 1 durante 1 clk para indicar nueva tecla recibida
    .scancode(scancode), // make o breakcode de la tecla
    .released(soltada),  // soltada=1, pulsada=0
    .extended(extendida)  // extendida=1, no extendida=0
  );  
  
  videosyncs sincronismos (
    .clk(clk),
    .hs(hs),
    .vs(vs),
	 .hblank(hblank),
 	  .hc(hc),
	  .vc(vc),
    .display_enable(den)
    );
  
  // Si hay algo que mostrar del módulo, éste tiene prioridad. Si no, se muestra lo que haya
  // en ri,gi,bi por parte del usuario. Si la señal den vale 0, entonces no se pinta nada.
  assign r = (den == 1'b1)?  ri : 6'h00;
  assign g = (den == 1'b1)?  gi : 6'h00;
  assign b = (den == 1'b1)? bi : 6'h00;
  
  assign posx = hc;
  assign posy = vc;
  assign display_activo = den;

endmodule

//////////////////////////////////////////////////////////////

module videosyncs (
  input wire clk,        // reloj de 25 MHz (mirar abajo en el "ModeLine")
  output reg hs,         // salida sincronismo horizontal
  output reg vs,         // salida sincronismo vertical
  output reg hblank,
 	output wire [10:0] hc, // salida posicion X actual de pantalla
	output wire [10:0] vc, // salida posicion Y actual de pantalla
  output reg display_enable // hay que poner un color en pantalla (1) o hay que poner negro (0)
  );
	
  // Visita esta URL si pretendes cambiar estos valores para generar otro modo de pantalla. Atrevete!!!
  // https://www.mythtv.org/wiki/Modeline_Database#VESA_ModePool
  // El que he usado aqui es:
  // ModeLine "640x480" 25.18 640 656 752 800 480 490 492 525 -HSync -VSync
  //                      ^
  //                      +---- Frecuencia de reloj de pixel en MHz
  parameter HACTIVE = 640;
  parameter HFRONTPORCH = 656;
  parameter HSYNCPULSE = 752;
	parameter HTOTAL = 800;
  parameter VACTIVE = 480;
  parameter VFRONTPORCH = 490;
  parameter VSYNCPULSE = 492;
  parameter VTOTAL = 525;
  parameter HSYNCPOL = 0;  // 0 = polaridad negativa, 1 = polaridad positiva
  parameter VSYNCPOL = 0;  // 0 = polaridad negativa, 1 = polaridad positiva

  reg [10:0] hcont = 0;
  reg [10:0] vcont = 0;
	
  assign hc = hcont;
  assign vc = vcont;

  always @(posedge clk) begin
      if (hcont == HTOTAL-1) begin
         hcont <= 11'd0;
         if (vcont == VTOTAL-1) begin
            vcont <= 11'd0;
         end
         else begin
            vcont <= vcont + 11'd1;
         end
      end
      else begin
         hcont <= hcont + 11'd1;
      end
  end
   
  always @* begin
    if (hcont>=0 && hcont<HACTIVE && vcont>=0 && vcont<VACTIVE)
      begin
		display_enable = 1'b1;
		hblank = 1'b1;
      end 
	 else
	   begin
      display_enable = 1'b0;
      hblank = 1'b0;
		end
    if (hcont>=HFRONTPORCH && hcont<HSYNCPULSE)
      hs = HSYNCPOL;
    else
      hs = ~HSYNCPOL;

    if (vcont>=VFRONTPORCH && vcont<VSYNCPULSE)
      vs = VSYNCPOL;
    else
      vs = ~VSYNCPOL;
  end
endmodule   

//////////////////////////////////////////////////////////////

module ps2_port (
    input wire clk,  // se recomienda 1 MHz <= clk <= 600 MHz
    input wire enable_rcv,  // habilitar la maquina de estados de recepcion
    input wire kb_or_mouse,  // 0: kb, 1: mouse
    input wire ps2clk_ext,
    input wire ps2data_ext,
    output wire kb_interrupt,  // a 1 durante 1 clk para indicar nueva tecla recibida
    output reg [7:0] scancode, // make o breakcode de la tecla
    output wire released,  // soltada=1, pulsada=0
    output wire extended  // extendida=1, no extendida=0
    );
    
    `define RCVSTART    2'b00
    `define RCVDATA     2'b01 
    `define RCVPARITY   2'b10
    `define RCVSTOP     2'b11

    reg [7:0] key = 8'h00;

    // Fase de sincronizacion de se�ales externas con el reloj del sistema
    reg [1:0] ps2clk_synchr;
    reg [1:0] ps2dat_synchr;
    wire ps2clk = ps2clk_synchr[1];
    wire ps2data = ps2dat_synchr[1];
    always @(posedge clk) begin
        ps2clk_synchr[0] <= ps2clk_ext;
        ps2clk_synchr[1] <= ps2clk_synchr[0];
        ps2dat_synchr[0] <= ps2data_ext;
        ps2dat_synchr[1] <= ps2dat_synchr[0];
    end

    // De-glitcher. S�lo detecto flanco de bajada
    reg [15:0] negedgedetect = 16'h0000;
    always @(posedge clk) begin
        negedgedetect <= {negedgedetect[14:0], ps2clk};
    end
    wire ps2clkedge = (negedgedetect == 16'hF000)? 1'b1 : 1'b0;
    
    // Paridad instant�nea de los bits recibidos
    wire paritycalculated = ^key;
    
    // Contador de time-out. Al llegar a 65536 ciclos sin que ocurra
    // un flanco de bajada en PS2CLK, volvemos al estado inicial
    reg [15:0] timeoutcnt = 16'h0000;

    reg [1:0] state = `RCVSTART;
    reg [1:0] regextended = 2'b00;
    reg [1:0] regreleased = 2'b00;
    reg rkb_interrupt = 1'b0;
    assign released = regreleased[1];
    assign extended = regextended[1];
    assign kb_interrupt = rkb_interrupt;
    
    always @(posedge clk) begin
        if (rkb_interrupt == 1'b1) begin
            rkb_interrupt <= 1'b0;
        end
        if (ps2clkedge && enable_rcv) begin
            timeoutcnt <= 16'h0000;
            case (state)
                `RCVSTART: begin
                    if (ps2data == 1'b0) begin
                        state <= `RCVDATA;
                        key <= 8'h80;
                    end
                end
                `RCVDATA: begin
                    key <= {ps2data, key[7:1]};
                    if (key[0] == 1'b1) begin
                        state <= `RCVPARITY;
                    end
                end
                `RCVPARITY: begin
                    if (ps2data^paritycalculated == 1'b1) begin
                        state <= `RCVSTOP;
                    end
                    else begin
                        state <= `RCVSTART;
                    end
                end
                `RCVSTOP: begin
                    state <= `RCVSTART;                
                    if (ps2data == 1'b1) begin                        
                        scancode <= key;
                        if (kb_or_mouse == 1'b1) begin
                            rkb_interrupt <= 1'b1;  // no se requiere mirar E0 o F0
                        end
                        else begin
                            if (key == 8'hE0) begin
                                regextended <= 2'b01;
                            end
                            else if (key == 8'hF0) begin
                                regreleased <= 2'b01;
                            end
                            else begin
                                regextended <= {regextended[0], 1'b0};
                                regreleased <= {regreleased[0], 1'b0};
                                rkb_interrupt <= 1'b1;
                            end
                        end
                    end
                end    
                default: state <= `RCVSTART;
            endcase
        end           
        else begin
            timeoutcnt <= timeoutcnt + 1;
            if (timeoutcnt == 16'hFFFF) begin
                state <= `RCVSTART;
            end
        end
    end
endmodule

`default_nettype wire
