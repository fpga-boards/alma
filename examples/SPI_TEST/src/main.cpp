#include <Arduino.h>
#include <SPI.h>

#ifndef RASPBERRY_PI_PICO
#ifdef ALTERNATE_PINS
  #define VSPI_MISO   2
  #define VSPI_MOSI   4
  #define VSPI_SCLK   0
  #define VSPI_SS     33

  #define HSPI_MISO   26
  #define HSPI_MOSI   27
  #define HSPI_SCLK   25
  #define HSPI_SS     32
#else
  #define VSPI_MISO   MISO
  #define VSPI_MOSI   MOSI
  #define VSPI_SCLK   SCK
  #define VSPI_SS     SS

  #define HSPI_MISO   12
  #define HSPI_MOSI   13
  #define HSPI_SCLK   14
  #define HSPI_SS     15
#endif
#else
  #define  MISO 12
  #define  MOSI 15
  #define  SCK  14
  #define  SS   13
#endif

#if ESP32
#define VSPI FSPI
#endif

#ifdef RASPBERRY_PI_PICO
#define LED 25
#else
#define LED 2
#endif

bool anotherValue=true;
static const int spiClk = 1000000; // 1 MHz

//uninitalised pointers to SPI objects
SPIClass * vspi = NULL;

typedef struct t_data{
  uint16_t x;
  uint16_t y;
  uint16_t color;
} ;

void spiCommand(SPIClass *spi, t_data *data) {
  uint16_t x,y;
  uint8_t received, receivedX, receivedY;
  
  char output[80];
  spi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE0));
  unsigned char myData[6]; //uint64_t myData = 0;
  for ( y =0;y<200;y++){
    for ( x =0;x<200;x++){
      data->x= x;
      data->y= y; 
      if (x>95 && x < 105)
        data->color=0xF800;
      else if (y>95 && y < 105)
        data->color=0x07E0;
      else
        data->color=0x001F; // 0000 0000 1111 0000 => ???? ???? GGGR RRRR => GGGB BBBB RRRR RGGG

      myData[5]= data->color >> 8;
      myData[4]= data->color;
      myData[3]= y;
      myData[2]= y >> 8;
      myData[1]= x;
      myData[0]= x >> 8;
      digitalWrite(SS, LOW); //pull SS slow to prep other end for transfer  
      spi->transfer(myData,6); //spiInstance->transfer(display->spiData->y);
      digitalWrite(SS, HIGH); //pull ss high to signify end of data transfer    
    }
  }
  spi->endTransaction();
}

void setup() {
  Serial.begin(UART_FREQ);
  Serial.println("SPI Master");

#if ESP32
  //initialise two instances of the SPIClass attached to VSPI and HSPI respectively
  vspi = new SPIClass(VSPI);
 #else
  vspi = new MbedSPI(MISO,MOSI,SCK);
 #endif 
  //clock miso mosi ss

#ifndef ALTERNATE_PINS
  //initialise vspi with default pins SCLK = 18, MISO = 19, MOSI = 23, SS = 5
  vspi->begin();
#else
  //alternatively route through GPIO pins of your choice
  vspi->begin(VSPI_SCLK, VSPI_MISO, VSPI_MOSI, VSPI_SS); //SCLK, MISO, MOSI, SS
#endif
  pinMode(SS, OUTPUT); 
  digitalWrite(SS, HIGH);

  pinMode(LED, OUTPUT); 

  // SPI test
  t_data mydata;
  spiCommand(vspi, &mydata);

}

// the loop function runs over and over again until power down or reset
void loop() {
  t_data mydata;

  anotherValue=!anotherValue;
  delay(100);
  digitalWrite(LED, HIGH);
  delay(100);
  digitalWrite(LED, LOW);
}
