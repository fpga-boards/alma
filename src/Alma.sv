/*
	This project generates 640x480 60Hz video.
*/
module Alma
(
	input logic clk12mhz,
	//output logic clkn,
	input logic rst,
	input logic up,
	input logic down,
	input logic left,
	input logic right,
	input logic fire1,
	input logic fire2,
	
	input SCK, 
	input SSEL, 
	input MOSI,
	output MISO,
		
 	output wire Out_TMDS_D0,		
	output wire Out_TMDS_D1,
	output wire Out_TMDS_D1_N,		
	output wire Out_TMDS_D2,		
	output wire Out_TMDS_CLK,

	output reg [7:0] led_on
);

logic clkPixel;
logic clkClock;
logic hSync, vSync, drawArea;
//logic [7:0] red, green, blue;
logic [9:0] TMDSRed, TMDSGreen, TMDSBlue;
logic [9:0] TMDSShiftRed, TMDSShiftGreen, TMDSShiftBlue;
reg [39:0] spiData, memData;
logic [15:0] memRAddr,memWAddr;
logic  memRead;
logic memWrite; 

localparam centerX = 10'd320; 
localparam centerY = 10'd240; 
localparam menuWidth = 10'd200; 
localparam menuHeight = 10'd200; 

localparam menuOriginX = centerX-menuWidth/2; 
localparam menuOriginY = centerY-menuHeight/2; 

wire [7:0] joy_data; // Connects the joy to the SPI module 

//// From modulos entrenamiento
	
 // assign hblank = hsync && vsync;
assign Out_TMDS_D1_N = 1'b0;
wire [5:0] r;
wire [5:0] g;
wire [5:0] b;
wire hsync;
wire vsync;
wire hblank;
wire display_enable;

wire clk25m;
wire clk125m;
wire clk420m; 

localparam COLOR_NEGRO   = {6'h00, 6'h00, 6'h00};   // pues eso, el negro	
localparam COLOR_VERDE = {6'h00, 6'h3F, 6'h00};   // verde

localparam COLOR_REJILLA = {6'h00, 6'h3F, 6'h00};   // verde
localparam COLOR_CRUZ    = {6'h3F, 6'h3F, 6'h3F};   // blanco

localparam CURSOR_ARRIBA = 8'h75;  //
localparam CURSOR_ABAJO  = 8'h72;  // scancodes de las teclas del cursor.
localparam CURSOR_IZQDA  = 8'h6B;  // Todas estas teclas son extendidas.
localparam CURSOR_DRCHA  = 8'h74;  //

reg [5:0] rojo, verde, azul;
wire [10:0] posx, posy;
wire display_activo;

reg [10:0] cruz_x = 10'd320;  // Registros que guardan la 
reg [10:0] cruz_y = 10'd240;  // posición de la cruceta
wire evento_teclado;
wire [7:0] codigo_tecla;
wire soltada, extendida;

always @(posedge clk25m) begin
 if (evento_teclado == 1'b1 && soltada == 1'b0 && extendida == 1'b1) begin  // si se ha pulsado una tecla extendida...
	case (codigo_tecla)                                                      // miramos a ver qué tecla es...
	  CURSOR_ARRIBA: if (cruz_y != 10'd0) cruz_y <= cruz_y - 10'd1;          // 
	  CURSOR_ABAJO:  if (cruz_y != 10'd479) cruz_y <= cruz_y + 10'd1;        // Y según qué tecla sea, se actualiza
	  CURSOR_IZQDA:  if (cruz_x != 10'd0) cruz_x <= cruz_x - 10'd1;          // cruz_x o cruz_y
	  CURSOR_DRCHA:  if (cruz_x != 10'd639) cruz_x <= cruz_x + 10'd1;        // 
	endcase
 end
end

always @* begin
 if (display_activo == 1'b1) begin
	if (posx >= menuOriginX && posx< menuOriginX+ menuWidth && posy >= menuOriginY && posy < menuOriginY + menuHeight)
		begin				
			memRAddr <= (16'd200*(posy-menuOriginY))+posx-menuOriginX;
			memRead <= 1'b1;
			rojo[5]<= memData[15];
			rojo[4]<= memData[14];
			rojo[3]<= memData[13];
			verde[5]<= memData[10];
			verde[4]<= memData[9];
			verde[3]<= memData[8];
			azul[5]<= memData[4];
			azul[4]<= memData[3];  
		end
	else
	  {rojo,verde,azul} = COLOR_NEGRO;
 end
 else
	{rojo,verde,azul} = COLOR_NEGRO;      
end


 relojes reloj25mhz (
 .inclk0(clk12mhz),
 .c0(clk25m),
 .c1(clk125m),
 .c2(clk420m)
 );

	
	
hdmi
//paso de ctes
//  #(25200000, 48000, 25200, 6144)
	
hdmi_18bits
 (
	//clocks
	.CLK_DVI_I(clk125m),	 
	.CLK_PIXEL_I(clk25m),		
	
	// components
	.R_I({r,2'b0}),				
	.G_I({g,2'b0}),				
	.B_I({b,2'b0}),				
	.BLANK_I(~hblank),			
	.HSYNC_I(hsync),			
	.VSYNC_I(vsync),			
	
	// PCM audio 
	//.I_AUDIO_PCM_L(), 	
	//.I_AUDIO_PCM_R(),	
	
	// TMDS output
	.TMDS_D0_O(Out_TMDS_D0),		
	.TMDS_D1_O(Out_TMDS_D1),		
	.TMDS_D2_O(Out_TMDS_D2),		
	.TMDS_CLK_O(Out_TMDS_CLK)
	);		
	
	

///// End From modulos entrenamiento

joystick joy
(
  .clk(clk25m),
  .rst(rst),
  .up(up),
  .down(down),
  .left(left),
  .right(right),
  .fire1(fire1),
  .fire2(fire2),
  .joy_data(joy_data) 
);

SPI_slave spi 
(
	.clk(clk125m),
	.SCK(SCK), 
	.MOSI(MOSI), 
	.MISO(MISO), 
	.SSEL(SSEL),
	.LED(led_on),
	.data(spiData),
	.writeToMem(memWrite), 
	.memAddr(memWAddr),
	.byte_to_send(joy_data)
);


 display #(.YINIT(480)) pantalla (  // la Y inicial la ponemos a un valor más allá del limite (479) para esconder el display
  // Acceso a la ultima tecla pulsada 
    .evento_teclado(),
    .scancode(),
    .soltada(),
    .extendida(),
  // Acceso a la pantalla
    .ri(rojo),
    .gi(verde),
    .bi(azul),
    .posx(posx),
    .posy(posy),
    .display_activo(display_activo),
    
/////////////////////////////////////////////////////////////////////////  
  // Interfaz externa. No debería tener que tocarse nada de lo que hay aquí.
    .clk(clk25m),
    .clkps2(clkps2),
    .dataps2(dataps2),
    .r(r),
    .g(g),
    .b(b),
	 .hblank(hblank),
    .hs(hSync),
    .vs(vSync)
    );

videoBuffer Buffer
(
	.rclock(clk420m),
	.wclock(clk420m),
	.read(memRead),
	.write(memWrite),
	.raddr(memRAddr),
	.rdata(memData),
	.waddr(memWAddr),
	.wdata(spiData)
);

endmodule 