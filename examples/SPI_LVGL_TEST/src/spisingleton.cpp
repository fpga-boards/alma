#include "spisingleton.h"

SPISingleton* SPISingleton::spiInstance = 0;


SPISingleton* SPISingleton::getInstance()
{
    if (spiInstance == 0)
    {
        spiInstance = new SPISingleton();
    }

    return spiInstance;
}

SPISingleton::SPISingleton(): SPI_CLASS(MISO,MOSI,SCK){
     this->begin();
}     