# Alma

## Description
This is an experimental core for the [ATLAS FPGA board](https://github.com/AtlasFPGA/CYC1000).

This core has been tested using a Trenz Cyc1000 and a Raspberry Pi Pico mounted on the Atlas FPGA. The Pico sends graphical information to the Cy1000 FPGA. The last one sends this information through the HDMI port and everything is displayed on a monitor or a TV.

### Important note
This is a work in progress project. Currently the drawing is slow and the software ([programrbf-maix](https://gitlab.com/deuteros/programrbf-maix)) used in the microcontroller is also immature.

## Requirements

## Hardware
- [Atlas carrier board](https://github.com/AtlasFPGA/CYC1000)
- Trenz Cyc1000
- Raspberry Pi Pico 
- [Raspberry Pi Pico adapter PCB for Atlas](https://gitlab.com/fpga-boards/atlas-pico-multicore-adapter)

Other microcontrollers such as ESP32 should work also assigning the correct pins.

## Software
- This is a hardware project but if you want to run something, you can try the examples of this repository, or use the [programrbf-maix](https://gitlab.com/deuteros/programrbf-maix) source code.
- PlatformIO (Arduino IDE should work also) for compiling the examples.
- The LVGL example requires the LVLG library for Arduino.

## Credits

This HDL code includes work from other people. Some modules are taken from the examples created or modified by the [Iniciativa Atlas](https://t.me/INICIATIVAATLAS) members.

